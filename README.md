## nuterm 

nuterm is a modified firmware for the uTerm (and uTerm-S) boards.
The uTerm boards are a great way to build a hardware terminal
with VGA output and a PS/2 keyboard input that you can connect to your
serial devices.

What is different about nuterm?
 * ported from Keil uVision compiler to GCC.
 * added a bugfix from https://github.com/gdampf/uTerm - sends keyboard reset code
 * fixed several keyboard logic bugs regarding SHIFT and NUMLOCK handling
 * added ability to send ALT- key combos (like ALT-x in EMACS)
 * added a menu mode that allows you to switch baud rates and data settings on the fly.
 * added ability to change backspace from Ctrl-H to Ctrl-? (in menu mode, or by default)
 * expanded the VT100/VT52 emulation to make it perform better with *nix programs, and Thomas Dickey's vttest.
 * changed font to IBM VGA 8x16 from the Ultimate Oldschool PC fontpack (https://int10h.org/).


Why?
 * I am not a Windows guy, so I wanted to port to Linux/open source tools.
 * As a learning experience.
 * To add some extra features to the firmware, and fix some bugs.

### Background 

uTerm and uTerm-S are circuit boards designed by Just4Fun that provide a terminal 
with VGA output and PS/2 keyboard input to a serial device.  The plain uTerm
connects with TTL level serial devices, and the uTerm-S has an RS-232 connector.

https://hackaday.io/project/165325 
https://github.com/SuperFabius/uTerm

uTerm was derived from the work of K. C. Lee who designed the
original ChibiTerm terminal, and firmware. 

https://github.com/FPGA-Computer/STM32F030F4-VGA 
https://hw-by-design.blogspot.com/2018/07/low-cost-vga-terminal-module-project.html

Madis Kaal modified the ChibiTerm firmware to include 
VT100 emulation, font editor, 16-line font, and support for inverse fonts

http://www.nomad.ee/micros/chibiterm.shtml

### Precompiled binaries

You can find several precompiled binaries ready for flashing:
 * nuterm-menu.bin (Menu Mode enabled)
 * nuterm-no_menu.bin (No menu mode, with a message showing starting baud rate)
 * nuterm-no_menu-no_startmsg.bin (self explanatory)

### Compiling

Note: I am developing this on Fedora 34 with arm-none-eabi-gcc-10.2.0

1. Edit the src/config.h file to choose appropriate options.
2. Run the ``make'' command in the root directory of this project.
3. Flash the resulting nuterm.bin file.  If flashing fails, try to reset the 
uTerm (with the reset button on the PCB), then flash again.  That works for me.


### License(s)

Source code distributed under GPLV3, with exception of fontedit.py that is
distributed under BSD license.
